package Bouttons;


import Controleur.ControleurTrace;
import Controleur.FabricantFigure;
import Controleur.ManipulateurForme;
import Model.DessinFigure;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by RAPHAEL & THOMAS on 28/04/2016.
 */
@SuppressWarnings("serial")
public class PanneauChoix extends JPanel {

    /**
     * Attributs
     */
	private static Color couleur = Color.BLACK;
	private static String figure ="";
    DessinFigure dess;
    ManipulateurForme mf;
    ControleurTrace ct ;
    FabricantFigure ff;


    public PanneauChoix(DessinFigure df){

        this.dess = df;
        //------------------*Les Boutons*-------------------\\
        JRadioButton b1 = new JRadioButton("Nouvelle Figure");
        JRadioButton b2 = new JRadioButton("Tracé à main levé");
        JRadioButton b3 = new JRadioButton("Manipulations");
        JButton b4 = new JButton("New");
        JButton beff = new JButton("Effacer");
        JButton b5 = new JButton("Exporter");

        JComboBox<String> figs = new JComboBox<>(new String[]{"Polygone","Rectangle","Carre","Cercle"});
        JButton colo = new JButton("Couleur");
        figs.setEnabled(false);
        colo.setEnabled(false);

        ButtonGroup bg = new ButtonGroup();
        bg.add(b1);
        bg.add(b2);
        bg.add(b3);

        this.setLayout(new BorderLayout());

        JPanel panelBouton = new JPanel();
        JPanel panelMenu = new JPanel();
        JPanel panelNorth = new JPanel();

        panelNorth.setLayout(new BorderLayout());
        panelBouton.add(b1);
        panelBouton.add(b2);
        panelBouton.add(b3);
        panelBouton.add(b4);
        panelBouton.add(beff);
        panelBouton.add(b5);
        panelMenu.add(colo);
        panelMenu.add(figs);
        panelNorth.add(panelBouton, BorderLayout.NORTH);
        panelNorth.add(panelMenu, BorderLayout.CENTER);
        this.add(panelNorth, BorderLayout.NORTH);


        //---*Methodes d'evenements lors du choix des boutons b1,b2,b3*---\\

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                figs.setEnabled(true);
                colo.setEnabled(true);
                df.removeAllListener();
                ff = new FabricantFigure();
                df.ajoutListener(ff);
            }
        });

       b2.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               //dess.supprimerAuditeur();
               figs.setEnabled(false);
               colo.setEnabled(true);
               df.removeAllListener();
               ct = new ControleurTrace();
               df.ajoutListener(ct);
           }
       });

        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                figs.setEnabled(false);
                colo.setEnabled(true);
                df.removeAllListener();
                mf = new ManipulateurForme();
                df.ajoutListener(mf);
            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bg.clearSelection();
                figs.setEnabled(false);
                colo.setEnabled(false);
                df.clearAll();
                df.removeAllListener();
            }
        });
        
        colo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				couleur = JColorChooser.showDialog(colo,"Choix couleur",Color.BLACK);
                colo.setForeground(couleur);
				
			}
		});
        figs.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				figure = (String) figs.getSelectedItem();
				
			}
		});

        beff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                df.clear();
                df.repaint();
            }
        });

        b5.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                df.sauvegarder();
            }
        });
    }

    /**
     * Methode qui permet de connaitre la couleur choisie
     * @return la couleur
     */
    public static Color determineCouleur(){
        return couleur;
    }

    /**
     * Methode qui permet de connaitre la figure choisie
     * @return la figure
     */
    public static String getFigure(){
    	return figure;
    }

}
