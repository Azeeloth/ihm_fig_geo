package Model;
import java.awt.*;
/**
 * Created by RAPHAEL & THOMAS on 04/05/2016.
 */
public class Polygone extends FigureColoree{


    int xPoints[];
    int yPoints[];
    int points;
    int nbPoints;

    public Polygone(int i){
        super();
        nbPoints = i;
        tab_mem = new Point[getNbPoints()];
    }

    public int getNbPoints(){
        return nbPoints;
    }

    /**
     *Methodes qui modifie les points du polygone
     * @param tabP un tableau de points
     */
    public void modifierPoints(Point[] tabP) {
        tab_mem = tabP;
        points = tab_mem.length;
        xPoints = new int[points];
        yPoints = new int[points];
        for(int i = 0; i< points; i++){
            xPoints[i] = tab_mem[i].getX();
            yPoints[i] = tab_mem[i].getY();
        }

    }

    /**
     *Methode qui translate le polygone
     * @param xi la nouvelle absicce voulue
     * @param yi la nouvelle ordonnee voulue
     */
    @Override
    public void Translater(int xi, int yi) {
        if(isSelected()){
            for (int i = 0; i < nbPoints; i++) {
                xPoints[i] += xi;
                yPoints[i] += yi;
                tab_mem[i].setX(tab_mem[i].getX()+xi);
                tab_mem[i].setY(tab_mem[i].getY()+yi);
            }
        }
    }

    /**
     * Methdoe d'affichage du Polygone
     * @param g
     */
    public void afficher(Graphics g){
        g.fillPolygon(this.xPoints, this.yPoints, this.points);
    }


    /**
     * Methode de l'API java "contains"
     * @param x abscisse du point
     * @param y ordonnee du point
     * @return true si c'est dedans, sinon false
     */
    public boolean estDedans(int x, int y) {
        int hits = 0;

        int lastx = xPoints[nbPoints - 1];
        int lasty = yPoints[nbPoints - 1];
        int curx, cury;

        for (int i = 0; i < nbPoints; lastx = curx, lasty = cury, i++) {
            curx = xPoints[i];
            cury = yPoints[i];
            if (cury == lasty) {
                continue;
            }

            int leftx;
            if (curx < lastx) {
                if (x >= lastx) {
                    continue;
                }
                leftx = curx;
            } else {
                if (x >= curx) {
                    continue;
                }
                leftx = lastx;
            }

            double test1, test2;
            if (cury < lasty) {
                if (y < cury || y >= lasty) {
                    continue;
                }
                if (x < leftx) {
                    hits++;
                    continue;
                }
                test1 = x - curx;
                test2 = y - cury;
            } else {
                if (y < lasty || y >= cury) {
                    continue;
                }
                if (x < leftx) {
                    hits++;
                    continue;
                }
                test1 = x - lastx;
                test2 = y - lasty;
            }

            if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
                hits++;
            }
        }

        return ((hits & 1) != 0);
    }
}
