package Model;

import Bouttons.PanneauChoix;

import java.awt.*;

public class Trace implements Traçable{
	/**
	 * Attributs
	 */
	int lastX, lastY, newX, newY;
	Color couleur;

    /**
     * Contructeru principal
     * @param x l'abscisse a l'origine du trace
     * @param y l'ordonnee a l'origine du trace
     * @param x2 l'abscisse a la fin du trace
     * @param y2 l'ordonnee a la fin du trace
     */
	public Trace(int x, int y, int x2, int y2){
		lastX = x;
		lastY = y;
		newX = x2;
		newY = y2;
		couleur = PanneauChoix.determineCouleur();
	}

	public int getLastX() {
		return lastX;
	}

	public int getLastY() {
		return lastY;
	}

	public int getNewX() {
		return newX;
	}

	public int getNewY() {
		return newY;
	}
	
	public Color getColor(){
		return couleur;
	}
}
