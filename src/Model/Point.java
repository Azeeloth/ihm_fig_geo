package Model;

/**
 * Created by RAPHAEL & THOMAS on 03/05/2016.
 */
public class Point {
    /**
     * Attributs
     */
    private int x,y;

    /**
     * Constructeur principal
     * @param xi abscisse du point
     * @param yi ordonnée du point
     */
    public Point(int xi, int yi){
        if(xi > 0)
            this.x = xi;
        else
            this.x = 0;
        if(yi > 0)
            this.y = yi;
        else
            this.y = 0;
    }


    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public void setX(int x1){
        this.x = x1;
    }

    public void setY(int y1){
        this.y = y1;
    }

    /**
     * Methode qui permet de calculer une distance
     * @param p2 le 2éme point
     * @return la distance
     */
    public  double calculerDistance(Point p2){
        return Math.sqrt(Math.pow(p2.getX()- this.getX(), 2)+Math.pow(p2.getY()- this.getY(), 2));
    }


}
