package Model;

import java.awt.*;

/**
 * Created by RAPHAEL & THOMAS on 04/05/2016.
 */
public class Rectangle extends Polygone {
    public Rectangle(){
        super(4);
    }

    /**
     * Methode d'affichage ( de création ) prevu pour le rectangle
     * @param g le Graphics
     */
    public void afficher(Graphics g){
        setCorner();
       int width = this.tab_mem[2].getX()-this.tab_mem[0].getX();
        int heigth = this.tab_mem[2].getY()-this.tab_mem[0].getY();

        g.fillRect(this.tab_mem[0].getX(), this.tab_mem[0].getY(),width , heigth);
    }

    /**
     * Methode fixe les points du rectangle
     */
    public void setCorner(){
        int width = this.tab_mem[2].getX()-this.tab_mem[0].getX();
        int heigth = this.tab_mem[2].getY()-this.tab_mem[0].getY();
        Point tab[] = new Point[getNbPoints()];
        tab[0] = tab_mem[0];
        tab[1] = new Point(this.tab_mem[0].getX()+width,this.tab_mem[0].getY());
        tab[3]= new Point(this.tab_mem[0].getX(),this.tab_mem[0].getY()+heigth);
        tab[2] = new Point(tab[1].getX(),tab[3].getY());

        modifierPoints(tab);
    }
}
