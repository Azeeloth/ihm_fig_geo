package Model;

import java.awt.*;

/**
 * Created by RAPHAEL & THOMAS on 23/05/2016.
 */
public class Cercle extends FigureColoree{

    private Point centre;
    private int rayon;

    public Cercle(){
        super();
    }


    /**
     * Methode pour recuperer le nombres de points necessaires
     * @return 2 points utils
     */
    @Override
    public int getNbPoints() {
        return 2;
    }

    /**
     * Methode qui permet de savoir si on se trouve dans le cercle,
     * ou non
     * @param x
     * @param y
     * @return
     */
    @Override
    public boolean estDedans(int x, int y) {
        double dist = centre.calculerDistance(new Point(x,y));
        if(dist > this.rayon)
            return false;
        else
            return true;
    }

    /**
     *
     * @param tabP tableau de points
     */
    @Override
    public void modifierPoints(Point[] tabP) {
        tab_mem = tabP;
        centre = tab_mem[0];
        rayon = (int)tab_mem[0].calculerDistance(tab_mem[1]);
    }

    /**
     * Methdoe pour translater le Cercle
     * @param xi la nouvelle absicce voulue
     * @param yi la nouvelle ordonnee voulue
     */
    @Override
    public void Translater(int xi, int yi) {
        Point tab[] = new Point[getNbPoints()];
        centre.setX(centre.getX()+xi);
        centre.setY(centre.getY()+yi);
        tab[0] = centre;
        tab[1]= new Point(centre.getX()+rayon,centre.getY());

        modifierPoints(tab);
    }

    /**
     * Methode qui permet d'afficher ( creer ) le Cercle
     * @param g
     */
    public void afficher(Graphics g){

        g.fillOval(centre.getX()-rayon,centre.getY()-rayon,2*rayon,2*rayon);

    }
}
