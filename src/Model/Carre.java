package Model;

import java.awt.*;

/**
 * Created by RAPHAEL & THOMAS on 04/05/2016.
 */
public class Carre extends Polygone {
    public Carre(){
        super(4);

    }


    /**
     * Methode d'affichage ( de creation ) prevu pour le Carré
     * @param g
     */
    public void afficher(Graphics g){
        setCorner();
        int width = (int)((this.tab_mem[0].calculerDistance((this.tab_mem[2])))/Math.sqrt(2));
        g.fillRect(this.tab_mem[0].getX(), this.tab_mem[0].getY(), width, width);
    }

    /**
     * Methode qui fixe les points du carre
     */
    public void setCorner(){
        int width = (int)((this.tab_mem[0].calculerDistance((this.tab_mem[2])))/Math.sqrt(2));
        Point tab[] = new Point[getNbPoints()];
        tab[0] = tab_mem[0];
        tab[1] = new Point(this.tab_mem[0].getX()+width,this.tab_mem[0].getY());
        tab[3]= new Point(this.tab_mem[0].getX(),this.tab_mem[0].getY()+width);
        tab[2] = new Point(tab[1].getX(),tab[3].getY());

        modifierPoints(tab);
    }
}
