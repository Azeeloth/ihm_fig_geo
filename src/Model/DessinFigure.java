package Model;

import Controleur.paintListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by RAPHAEL & THOMAS on 28/04/2016 at 14:58.
 */
@SuppressWarnings("serial")
public class DessinFigure extends JPanel{
	
	/**
     * Attributs
	 */


    private int nbFigure;
	private List<Traçable> tab;
    private Trace tempTrace[]= {new Trace(0,0,0,0)};
    private FigureColoree tempPoly[];
    private List<paintListener> listeListener;

	public DessinFigure(){
		tab = new ArrayList<>();
        removeTemp();
        listeListener = new ArrayList<>();
	}

    /**
     * Methode qui gère l'aspect graphique de notre projet
     * @param g le Graphics
     */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		for(Traçable t : tab){
            if(t instanceof Trace){
                drawTrace(g, (Trace)t);
            }else if(t instanceof FigureColoree){
                drawFig(g, (FigureColoree)t);
            }
		}
        drawTemp(g);
        removeTemp();
	}

    /**
     * Methode qui permet de remttre a 0 nos variables temporaires
     */
    private void removeTemp() {
        Polygone mdr = new Polygone(0);
        Point tab[]={new Point(0,0),new Point(0,0),new Point(0,0),new Point(0,0)};
        mdr.modifierPoints(tab);
        tempPoly = new FigureColoree[1];
        tempPoly[0] = mdr;
        tempTrace[0] = new Trace(0,0,0,0);
    }

    /**
     * Methode qui permet de tracer à main levé
     * @param g le Graphics
     * @param t notre objet trace
     */
    private void drawTrace(Graphics g, Trace t){
		g.setColor(t.getColor());
		g.drawLine(t.getLastX(), t.getLastY(), t.getNewX(), t.getNewY());
	}

    /**
     * Methode qui permet d'afficher une figure
     * @param g le Graphics
     * @param p notre figure
     */
    private void drawFig(Graphics g, FigureColoree p){
        g.setColor(p.getColor());
        p.afficher(g);
    }
    /**
     * Methode qui permet de stocker dans un tableau temporaire,
     * nos tracés, et nos figures
     * @param g le Graphics
     */
    private void drawTemp(Graphics g){
		drawTrace(g, tempTrace[0]);
        drawFig(g, tempPoly[0]);
	}

    /**
     *Methode qui ajoute un nouveau trace
     * @param t le nouveau trace
     */
	public void ajoutTrace(Trace t){
		tab.add(t);
        nbFigure ++;
	}

    /**
     *Methode qui ajoute une nouvelle figure colorée
     * @param p la nouvelle figure
     */
    public void ajouterPoly(FigureColoree p){
        tab.add(p);
        nbFigure ++;
    }

    /**
     * Methdoe qui permet d'ajouter une figure dans notre tableau temporaire
     * @param p notre figure
     */
    public void ajoutTempPoly(FigureColoree p){
        tempPoly[0] = p;
    }

    /**
     * Methode qui permet d'ajouter un tracé dans notre tableau temporaire
     * @param t notre tracé
     */
    public void ajoutTemp(Trace t){
        tempTrace[0] = t;
    }

    /**
     * Methode qui permet de supprimer 1 par 1 les figures présentes
     * (et Tracés)
     */
	public void clear(){
        if(nbFigure -1 >= 0) {
            tab.remove(nbFigure - 1);
            nbFigure--;
        }
    }

    /**
     * Methode qui permet de tout supprimer d'un coup
     */
    public void clearAll(){
        this.nbFigure = 0;
        tab.clear();
        this.removeTemp();
        repaint();
    }

    /**
     * Methode qui permet de sauvegarder votre chef-d'oeuvre en png !
     */
    public void sauvegarder(){
        BufferedImage bImg = new BufferedImage(this.getWidth(),this.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bImg.createGraphics();
        this.paintAll(g);
        JFileChooser jfc = new JFileChooser();
        int saveValue = jfc.showSaveDialog(null);
        if (saveValue == JFileChooser.APPROVE_OPTION) {
            try {
                if (ImageIO.write(bImg, "png",jfc.getSelectedFile())) {
                    System.out.println("--- Sauvegarder");
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


    }

    /**
     * Methode qui retourne le tableau de dessin
     * @return le tableau de dessin
     */
    public List<Traçable> getTab(){
        return tab;
    }

    /**
     * Methode qui enleve tous les listener
     */
    public void removeAllListener() {
        for(int i = 0; i< listeListener.size(); i++){
            removeMouseListener((MouseListener)listeListener.get(i));
            removeMouseMotionListener((MouseMotionListener)listeListener.get(i));
            listeListener.remove(i);
        }
    }

    /**
     * Methode qui permet d'ajouter des listener
     * @param pl le nouveau listener
     */
    public void ajoutListener(paintListener pl) {
        this.addMouseMotionListener((MouseMotionListener)pl);
        this.addMouseListener((MouseListener)pl);
        pl.setEnable(true);
        listeListener.add(pl);
    }
}
