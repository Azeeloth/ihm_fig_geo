package Model;

import Bouttons.PanneauChoix;

import java.awt.*;

/**
 * Created by RAPHAEL & THOMAS on 03/05/2016.
 */
public abstract class FigureColoree implements Traçable{

    /**
     * Attributs
     */

    private static int TAILLE_CARRE_SELECTION=10; // Pour les petits carrés dans le coin de la figure lors d'une selection
    private boolean selected;
    protected Color couleur;
    protected Point[] tab_mem;

    public FigureColoree() {
        //initialisation des atributs (généraux).
        couleur = PanneauChoix.determineCouleur();
        selected = false;
        tab_mem = new Point[getNbPoints()];
    }

    public abstract int getNbPoints();

    public abstract boolean estDedans(int x, int y);

    public abstract void modifierPoints(Point[] tabP);


    /**
     * Methode qui affiche un DessinFigure
     * @param g une figure de DessinFigure
     */
    public void affiche(Graphics g){
        if(selected){
            for(int i=0; i<tab_mem.length;i++){
                g.setColor(new Color(255, 255, 255));
                g.fillRect(tab_mem[i].getX()-(TAILLE_CARRE_SELECTION/2),
                        tab_mem[i].getY()-(TAILLE_CARRE_SELECTION/2),
                        TAILLE_CARRE_SELECTION, TAILLE_CARRE_SELECTION);
                g.setColor(new Color(0, 0, 0));
                g.drawRect(tab_mem[i].getX()-(TAILLE_CARRE_SELECTION/2),
                        tab_mem[i].getY()-(TAILLE_CARRE_SELECTION/2),
                        TAILLE_CARRE_SELECTION, TAILLE_CARRE_SELECTION);
            }
        }
    }

    /**
     * Methode de translation
     * @param xi la nouvelle absicce voulue
     * @param yi la nouvelle ordonnee voulue
     */
    public abstract void Translater(int xi, int yi);

    /**
     * Methode qui permet d'afficher du contenu
     * @param g
     */
    public abstract void afficher(Graphics g);

    /**
     * Methode qui permet de selectionner une figure
     */
    public void selection(){
        selected = true;
    }

    /**
     * Methode qui permet de deselectionner une figure
     */
    public void deSelection(){
        selected = false;
    }

    /**
     * Methode qui permet de connaitre la couleur
     * @return la couleur
     */
    public Color getColor(){
        return couleur;
    }

    /**
     * Methode boolenne qui permet de savoir si la figure est bien selctionnée
     * ou non
     * @return
     */
    public boolean isSelected(){return this.selected;}

    /**
     * Methode qui permet le changement de la couleur
     * @param c la nouvelle couleur
     */
    public void changeColor(Color c){
        couleur = c;
    }

    /**
     * methode qui retourne le tableau de points
     * @return le tableau de points
     */
    public Point[] getTab_mem(){
        return tab_mem;
    }

}