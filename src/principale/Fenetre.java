package principale;

import Bouttons.PanneauChoix;
import Model.DessinFigure;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class Fenetre extends JFrame{
	
	/**
	 * Attributs
	 */
	DessinFigure dessin;
	PanneauChoix menu;
	
	/**
	 * Constructeur de la fenetre
	 * @param s Nom de la fenetre
	 * @param w Largeur de la fenetre
	 * @param h Hauteur de la fenetre
	 */
	public Fenetre(String s, int w, int h){
		super(s);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		dessin = new DessinFigure();
		menu = new PanneauChoix(dessin);

		JPanel contenant = new JPanel();
		
		contenant.setLayout(new BorderLayout());
		contenant.setPreferredSize(new Dimension(w,h));
		contenant.add(dessin, BorderLayout.CENTER);
		contenant.add(menu, BorderLayout.NORTH);

		
		this.setContentPane(contenant);
		pack();
	}
}
