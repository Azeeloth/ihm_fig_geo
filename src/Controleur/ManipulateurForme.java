package Controleur;

import Bouttons.PanneauChoix;
import Model.*;
import Model.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;

/**
 * Created by RAPHAEL & THOMAS on 03/05/2016.
 */
public class ManipulateurForme implements MouseListener, MouseMotionListener, paintListener {

    int lastX, lastY;
    private boolean enable = false;


    List<Traçable> tab;
    int indiceSelec = -1;
    Color couleurAvantManip = PanneauChoix.determineCouleur();
    Point[] tab2;
    int lastFig = -1;

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (enable) {
            lastX = e.getX();
            lastY = e.getY();
            if (SwingUtilities.isLeftMouseButton(e)) {
                for (int i = 0; i < tab.size(); i++) {
                    if (tab.get(i) instanceof FigureColoree) {
                        if (((FigureColoree) tab.get(i)).estDedans(e.getX(), e.getY())) {
                            indiceSelec = i;
                            lastFig = i;
                            tab2 = ((FigureColoree) tab.get(lastFig)).getTab_mem();
                            break;
                        } else {
                            indiceSelec = -1;
                        }
                    }
                }
                if (indiceSelec != -1) {
                    ((FigureColoree) tab.get(indiceSelec)).selection();
                    ((FigureColoree) tab.get(indiceSelec)).affiche(((DessinFigure) e.getSource()).getGraphics());

                } else {
                    for (Traçable t : tab) {
                        if (t instanceof FigureColoree) {
                            ((FigureColoree) t).deSelection();
                            ((DessinFigure) e.getSource()).repaint();

                        }
                    }
                }
                if (couleurAvantManip != PanneauChoix.determineCouleur() && indiceSelec != -1) {
                    ((FigureColoree) tab.get(indiceSelec)).changeColor(PanneauChoix.determineCouleur());
                    ((DessinFigure) e.getSource()).repaint();
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (enable) {
            if (indiceSelec != -1) {
                ((FigureColoree) tab.get(indiceSelec)).affiche(((DessinFigure) e.getSource()).getGraphics());
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (enable) {
            if(lastFig != -1) {
                ((FigureColoree) tab.get(lastFig)).selection();
                ((FigureColoree) tab.get(lastFig)).affiche(((DessinFigure) e.getSource()).getGraphics());
                for (int i = 0; i < tab2.length; i++) {
                    if ((((e.getX() > tab2[i].getX() - 15) && (e.getX() < tab2[i].getX() + 15))
                            && ((e.getY() > tab2[i].getY() - 15) && (e.getY() < tab2[i].getY() + 15)))) {
                        tab2[i].setX(e.getX());
                        tab2[i].setY(e.getY());
                        ((FigureColoree) tab.get(lastFig)).modifierPoints(tab2);
                        ((DessinFigure) e.getSource()).repaint();
                    }
                }
            }
            if (indiceSelec != -1) {
                    ((FigureColoree) tab.get(indiceSelec)).Translater(e.getX() - lastX, e.getY() - lastY);
                    lastX = e.getX();
                    lastY = e.getY();
                    ((DessinFigure) e.getSource()).repaint();

            }
        }
    }


    @Override
    public void mouseMoved(MouseEvent e) {
        if(enable){
            tab = ((DessinFigure)e.getSource()).getTab();
        }
    }

    public void setEnable(boolean b){
        enable = b;
    }

}
