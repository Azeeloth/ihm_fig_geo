package Controleur;

import Model.DessinFigure;
import Model.Trace;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by RAPHAEL & THOMAS on 03/05/2016.
 */

public class ControleurTrace implements MouseListener, MouseMotionListener,paintListener {

	private int lastX;
	private int lastY;
	private String bouttonType;

	private boolean enable;

	@Override
	public void mouseDragged(MouseEvent e) {
		if (enable) {
			switch (bouttonType) {
			case "Left":
				((DessinFigure) e.getSource()).ajoutTrace(new Trace(lastX, lastY, e.getX(), e.getY()));
				lastX = e.getX();
				lastY = e.getY();
				((DessinFigure) e.getSource()).repaint();
				break;
			case "Right":
				Trace temp = new Trace(lastX, lastY, e.getX(), e.getY());
				((DessinFigure) e.getSource()).ajoutTemp(temp);
				((DessinFigure) e.getSource()).repaint();
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (enable) {
			if (SwingUtilities.isLeftMouseButton(e)) {
				bouttonType = "Left";
				lastX = e.getX();
				lastY = e.getY();

			} else if (SwingUtilities.isMiddleMouseButton(e)) {
				bouttonType = "Middle";
				((DessinFigure) e.getSource()).clear();
				((DessinFigure) e.getSource()).repaint();

			} else if (SwingUtilities.isRightMouseButton(e)) {
				bouttonType = "Right";
				lastX = e.getX();
				lastY = e.getY();
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (enable) {
			switch (bouttonType) {
			case "Right":
				((DessinFigure) e.getSource()).ajoutTrace(new Trace(lastX, lastY, e.getX(), e.getY()));
				lastX = e.getX();
				lastY = e.getY();
				((DessinFigure) e.getSource()).repaint();
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	
	public void setEnable(boolean b){
		enable = b;
	}
}
