package Controleur;

import Bouttons.PanneauChoix;
import Model.*;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

/**
 * Created by RAPHAEL & THOMAS on 04/05/2016.
 */
public class FabricantFigure implements MouseListener, MouseMotionListener, paintListener {


    private FigureColoree p;
    private Point[] tab;
    private int nbClicks = 0;
    private FigureColoree temp;
    private int posX;
    private int posY;
    private ArrayList<Point> tabPoly = new ArrayList<>();

    private boolean enable;

    @Override
    public void mouseClicked(MouseEvent e) {
        if (enable) {
            if (SwingUtilities.isMiddleMouseButton(e)) {
                ((DessinFigure) e.getSource()).clear();
                ((DessinFigure) e.getSource()).repaint();
                p = new Polygone(0);
                temp = new Polygone(0);
                nbClicks = 0;
                posX = 0;
                posY = 0;
                tabPoly = new ArrayList<>();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (enable) {
            posX = e.getX();
            posY = e.getY();
            if (SwingUtilities.isLeftMouseButton(e)) {
                switch (PanneauChoix.getFigure()) {
                    default:
                        break;
                    case "Polygone":
                        nbClicks ++;
                        tabPoly.add(new Point(e.getX(),e.getY()));
                        temp = new Polygone(nbClicks);
                        this.traceFigure(temp,e);
                        ((DessinFigure) e.getSource()).ajoutTempPoly(temp);
                        break;
                }
            }
            if(SwingUtilities.isRightMouseButton(e)){
                switch (PanneauChoix.getFigure()){
                    case "Polygone":
                        p = new Polygone(nbClicks);
                        this.traceFigure(p,e);
                        ((DessinFigure) e.getSource()).ajouterPoly(p);
                        tabPoly = new ArrayList<>();
                        nbClicks = 0;
                        break;
                    default:
                        break;

                }
            }
        }
    }


    @Override
    public void mouseReleased(MouseEvent e) {
        if(enable){
            if(SwingUtilities.isLeftMouseButton(e)){
                switch (PanneauChoix.getFigure()) {
                    default:
                        break;
                    case "Rectangle":
                        temp = new Rectangle();
                        Point[] tab = new Point[temp.getNbPoints()];
                        tab[0]=new Point(posX,posY);
                        tab[2]=new Point(e.getX(), e.getY());
                        tab[1]=new Point(e.getX(), posY);
                        tab[3]=new Point(posX, e.getY());
                        temp.modifierPoints(tab);
                        ((DessinFigure) e.getSource()).ajouterPoly(temp);
                        ((DessinFigure) e.getSource()).repaint();
                        break;
                    case "Carre":
                        temp = new Carre();
                        Point[] tab2 = new Point[temp.getNbPoints()];
                        tab2[0]=new Point(posX,posY);
                        tab2[2]=new Point(e.getX(), e.getY());
                        tab2[1]=new Point(e.getX(),posY);
                        tab2[3]=new Point(posX,e.getY());
                        temp.modifierPoints(tab2);
                        ((DessinFigure) e.getSource()).ajouterPoly(temp);
                        ((DessinFigure) e.getSource()).repaint();
                        break;
                    case  "Cercle":
                        Point centre = new Point(posX, posY);
                        temp = new Cercle();
                        Point tab3[] = new Point[temp.getNbPoints()];
                        tab3[0] = centre;
                        tab3[1]= new Point(e.getX(), e.getY());
                        temp.modifierPoints(tab3);
                        ((DessinFigure) e.getSource()).ajouterPoly(temp);
                        ((DessinFigure) e.getSource()).repaint();
                        break;
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void setEnable(boolean b) {
        enable = b;
    }

    private void traceFigure(FigureColoree p, MouseEvent e) {
        tab = new Point[p.getNbPoints()];
        int i = 0;
        for (Point pnt : tabPoly) {
            tab[i] = pnt;
            i++;
        }
        p.modifierPoints(tab);
        ((DessinFigure) e.getSource()).repaint();

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(enable){
            if(SwingUtilities.isLeftMouseButton(e)){
                switch (PanneauChoix.getFigure()) {
                    default:
                        break;
                    case "Rectangle":
                        temp = new Rectangle();
                        Point[] tab = new Point[temp.getNbPoints()];
                        tab[0]=new Point(posX,posY);
                        tab[2]=new Point(e.getX(), e.getY());
                        tab[1]= new Point(0,0);
                        tab[3]= new Point(0,0);
                        temp.modifierPoints(tab);
                        ((DessinFigure) e.getSource()).ajoutTempPoly(temp);
                        ((DessinFigure) e.getSource()).repaint();
                        break;
                    case "Carre":
                        temp = new Carre();
                        Point[] tab2 = new Point[temp.getNbPoints()];
                        tab2[0]=new Point(posX,posY);
                        tab2[2]=new Point(e.getX(), e.getY());
                        tab2[1]=new Point(0,0);
                        tab2[3]=new Point(0,0);
                        temp.modifierPoints(tab2);
                        ((DessinFigure) e.getSource()).ajoutTempPoly(temp);
                        ((DessinFigure) e.getSource()).repaint();
                        break;
                    case  "Cercle":
                        Point centre = new Point(posX, posY);
                        temp = new Cercle();
                        Point tab3[] = new Point[temp.getNbPoints()];
                        tab3[0] = centre;
                        tab3[1]= new Point(e.getX(), e.getY());
                        temp.modifierPoints(tab3);
                        ((DessinFigure) e.getSource()).ajoutTempPoly(temp);
                        ((DessinFigure) e.getSource()).repaint();
                        break;
                }
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }
}
